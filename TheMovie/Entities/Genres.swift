//
//  Genres.swift
//  TheMovie
//
//  Created by Denny Purwana on 2/5/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import Foundation

struct Genres: Codable {
    let genres: [Genre]?
}

struct Genre: Codable {
    let id: Int?
    let name: String?
}
