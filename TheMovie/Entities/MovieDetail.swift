//
//  MovieDetail.swift
//  TheMovie
//
//  Created by Denny Purwana on 2/5/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import Foundation

struct MovieDetail: Codable {
    let id: Int?
    let adult: Bool?
    let name: String?
    let backdrop_path: String?
    let belongs_to_collection: BelongToCollection?
    let genres: [Genre]?
    let original_language: String?
    let original_title: String?
    let overview: String?
    let popularity: Double?
    let poster_path: String?
    let release_date: String?
    let title: String?
    let video: Bool?
    let vote_average: Double?
    let vote_count: Int?
    let budget: Int?
    let homepage: String?
    let imdb_id: String?
    let revenue: Int?
    let status: String?
    let tagline: String?
}

struct BelongToCollection: Codable {
    let id: Int?
    let name: String?
    let poster_path: String?
    let backdrop_path: String?
}

