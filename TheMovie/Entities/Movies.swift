//
//  Movies.swift
//  TheMovie
//
//  Created by Denny Purwana on 2/5/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import Foundation

struct Movies: Codable {
    let page : Int?
    let results: [Movie]?
    let total_pages: Int?
}

struct Movie: Codable {
    let id: Int?
    let adult: Bool?
    let name: String?
    let backdrop_path: String?
    let genre_ids: [Int]?
    let original_language: String?
    let original_title: String?
    let overview: String?
    let popularity: Double?
    let poster_path: String?
    let release_date: String?
    let title: String?
    let video: Bool?
    let vote_average: Double?
    let vote_count: Int?
}
