//
//  CollectionViewDataSource.swift
//  TheMovie
//
//  Created by Denny Purwana on 2/10/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import Foundation
import UIKit

protocol CollectionViewDataSource: AnyObject {
   
    var numberOfItems: Int { get }
    func itemCell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell
    func didSelect(collectionView: UICollectionView, indexPath: IndexPath)
    
}
