//
//  TableViewItemDataSource.swift
//  TheMovie
//
//  Created by Denny Purwana on 2/10/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import Foundation
import UIKit

protocol TableViewItemDataSource: AnyObject {
    
    var numberOfItems: Int { get }
    func itemCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell
    func didSelect(tableView: UITableView, indexPath: IndexPath)
    
}
