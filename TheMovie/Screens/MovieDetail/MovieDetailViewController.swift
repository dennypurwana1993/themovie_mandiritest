//
//  MovieDetailViewController.swift
//  TheMovie
//
//  Created by Denny Purwana on 2/5/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import UIKit
import Kingfisher
class MovieDetailViewController: UIViewController , MovieDetailView
,UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout{
    
    @IBOutlet weak var openAllReview: UILabel!
    @IBOutlet weak var iconBack: UIImageView!
    @IBOutlet weak var reviewsCollectionView: UICollectionView!
    @IBOutlet weak var revenueLbl: UILabel!
    @IBOutlet weak var budgetLbl: UILabel!
    @IBOutlet weak var genreLbl: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var overviewLbl: UILabel!
    @IBOutlet weak var taglineLbl: UILabel!
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var backdropImageView: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    var presenter = MovieDetailPresenter(service: MovieDetailService(), reviewService: ReviewsService())
    var genreArray : [String] = []
    var reviews: [Review] = []
    var movieId : Int = 0
    var movieTitle: String = ""
    var movieOverview: String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
        setupCollectionView()
        print("MOVIE ID \(movieId)")
        presenter.loadMovieDetail(movieId: movieId)
        presenter.loadMovieReviews(movieId: movieId)
    }
    
    func showReviews(data: [Review]?) {
        if(data!.count > 0){
            self.reviews = data ?? []
            self.reviewsCollectionView.reloadData()
        }
    }
    
    func showMovieDetail(movie: MovieDetail?) {
        let imageUrlBackdrop =  "\(Config.BaseUrlImages)\(movie?.backdrop_path ?? "")"
        backdropImageView.kf.setImage(with: URL(string: imageUrlBackdrop), placeholder: UIImage(named: "clapperboard"))
        let imageUrlPoster =  "\(Config.BaseUrlImages)\(movie?.poster_path ?? "")"
        posterImageView.kf.setImage(with: URL(string: imageUrlPoster), placeholder: UIImage(named: "clapperboard"))
        self.movieTitle = movie?.title ?? ""
        self.movieOverview = movie?.overview ?? ""
        titleLbl.text = movie?.title
        taglineLbl.text = movie?.tagline
        overviewLbl.text = movie?.overview
        statusLbl.text = movie?.status
        budgetLbl.text = dollarsFormat(data: movie?.budget! as! NSNumber)
        revenueLbl.text = dollarsFormat(data: movie?.revenue! as! NSNumber)
        statusView.layer.cornerRadius = 5
        statusView.layer.masksToBounds = true
        if let genres = movie?.genres,
            let genre = genres as? [Genre]
        {
            for object in genre {
                let name = object.name
                genreArray.append("\(name ?? "")")
            }
        }
        genreLbl.text = genreArray.joined(separator: " , ")
    }
    
    
    
    func setupCollectionView(){
        
        
        registerNib(collectionView: reviewsCollectionView, nibName:"ReviewsCollectionViewCell", identifierName:"reviewsCell")
        let gestureToAllReviews = UITapGestureRecognizer(target: self, action:  #selector(self.openReviews))
        openAllReview.isUserInteractionEnabled = true
        openAllReview.addGestureRecognizer(gestureToAllReviews)
        
        let gestureBack = UITapGestureRecognizer(target: self, action:  #selector(self.back))
        iconBack.isUserInteractionEnabled = true
        iconBack.addGestureRecognizer(gestureBack)
    }
    
    @objc func back(){
        //self.dismiss(animated: true, completion: nil)
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromLeft
        transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc func openReviews(){
        showAllReviewsController(movieId: movieId, movieTitle: self.movieTitle, overview: self.movieOverview)
    }
    
    
    
    
    
    func registerNib(collectionView:UICollectionView, nibName:String, identifierName:String){
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib.init(nibName: nibName, bundle: nil), forCellWithReuseIdentifier: identifierName)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return reviews.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "reviewsCell", for: indexPath) as! ReviewsCollectionViewCell
        let data = reviews[indexPath.row]
        cell.configure(with: data)
        return cell
        
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: (collectionView.bounds.width), height: 250 )
        
    }
    
    func dollarsFormat(data: NSNumber) -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.groupingSeparator = ","
        numberFormatter.numberStyle = .decimal
        numberFormatter.maximumFractionDigits = 20
        
        let rupiah = "$" + (numberFormatter.string(from: data) ?? "0")
        return rupiah
    }
    func startLoading() {
        
    }
    
    func stopLoading() {
        
    }
    
    func failed(code: String) {
        
    }
    
}


extension UIViewController {
    
    func showMovieDetailController(movieId: Int) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Dashboard", bundle:nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "movieDetail") as! MovieDetailViewController
        vc.hidesBottomBarWhenPushed = true
        vc.modalPresentationStyle = .fullScreen
        vc.movieId = movieId
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromRight
        transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
        
        present(vc, animated: false, completion: nil)
    }
    
}
