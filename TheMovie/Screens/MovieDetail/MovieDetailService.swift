//
//  MovieDetailService.swift
//  TheMovie
//
//  Created by Denny Purwana on 2/5/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import Foundation
import RxSwift
import Kingfisher

class MovieDetailService: NSObject {
    
    let disposeBag = DisposeBag()
    
    func loadMovieDetail(movieID:Int, successCallback: ((_ result: MovieDetail?) -> Void)?,
                                  errorCallback: ((_ code: String) -> Void)?) {
        provider.rx.request(.movieDetail(movieID))
            .subscribe { (event) in
                switch event {
                case .success(let result):
                    do {
                        if let JSONString = String(data: result.data, encoding: String.Encoding.utf8) {
                         //   print(JSONString)
                        }
                        let response = try JSONDecoder().decode(MovieDetail.self, from: result.data)
//                        print(response)
                        successCallback?(response)
                    }
                    catch {
                        print(error.localizedDescription)
                    }
                case .error(let error):
                    print(error.localizedDescription)
                }
        }
        .disposed(by: disposeBag)
    }
    
}
