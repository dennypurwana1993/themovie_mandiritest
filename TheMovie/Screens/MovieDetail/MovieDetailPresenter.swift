//
//  MovieDetailPresenter.swift
//  TheMovie
//
//  Created by Denny Purwana on 2/5/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import Foundation
import UIKit

class MovieDetailPresenter: NSObject {
    
    private let service: MovieDetailService
    private let reviewService: ReviewsService
    weak private var view: MovieDetailView?
    init(service: MovieDetailService, reviewService: ReviewsService) {
        self.service = service
        self.reviewService = reviewService
    }
    
    func attachView(view: MovieDetailView) {
        self.view = view
    }
    
    func detachView() {
        view = nil
    }
    
    func loadMovieDetail(movieId: Int) {
       self.view?.startLoading()
        self.service.loadMovieDetail(movieID: movieId,
            successCallback: {(data) in
                self.view?.showMovieDetail(movie: data)
        },
            errorCallback: { (errorMessage) in
                self.view?.stopLoading()
        })
    }
    
    func loadMovieReviews(movieId: Int) {
       self.view?.startLoading()
        self.reviewService.loadAllReviews(movieID: movieId,
            successCallback: {(data) in
            self.view?.showReviews(data: data)
        },
            errorCallback: { (errorMessage) in
                self.view?.stopLoading()
        })
    }
    
       
}
