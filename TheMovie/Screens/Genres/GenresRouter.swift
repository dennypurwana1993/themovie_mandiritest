//
//  GenresRouter.swift
//  TheMovie
//
//  Created by Denny Purwana on 2/10/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import Foundation

import Foundation
import UIKit

struct GenresRouterInput {

    private func view() -> GenresViewController {
        let view = GenresViewController()
        let interactor = GenresInteractor()
        let dependencies = GenresPresenterDependencies(interactor: interactor, router: GenresRouterOutput(view))
        let presenter = GenresPresenter(entities: GenreEntities(), view: view, dependencies: dependencies)
        view.presenter = presenter
        view.collectionViewDataSource = GenresCollectionViewDataSource(entities: presenter.entities, presenter: presenter)
        interactor.presenter = presenter
        return view
    }

    func push(from: Viewable) {
        let view = self.view()
        from.push(view, animated: true)
    }

    func present(from: Viewable) {
        let nav = UINavigationController(rootViewController: view())
        from.present(nav, animated: true)
    }
}

final class GenresRouterOutput: Routerable {

    private(set) weak var view: Viewable!

    init(_ view: Viewable) {
        self.view = view
    }
}
