//
//  GenreEntities.swift
//  TheMovie
//
//  Created by Denny Purwana on 2/10/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import Foundation

final class GenreEntities {
    var genres: [Genre] = []
    
    class GenreApiState {
        var pageCount = 1
        var isFetching = false
    }
    
    var genreApiState = GenreApiState()
}
