//
//  GenresCollectionViewDataSource.swift
//  TheMovie
//
//  Created by Denny Purwana on 2/10/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import Foundation
import UIKit

protocol GenresCollectionViewDataSourceOutputs: AnyObject {
    func didSelect(_ genre: Genre)
}

final class GenresCollectionViewDataSource: CollectionViewDataSource {
    
    private weak var entities: GenreEntities!
    private weak var presenter: GenresCollectionViewDataSourceOutputs?
    var selectedIndex = Int ()
    
    init(entities: GenreEntities, presenter: GenresCollectionViewDataSourceOutputs) {
        self.entities = entities
        self.presenter = presenter
    }
    
    var numberOfItems: Int {
        return entities.genres.count
    }
    
    func itemCell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        let genre = entities.genres[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "genreCell", for: indexPath) as! GenreCollectionViewCell
        if selectedIndex == indexPath.row
        {
            cell.titleBackground.backgroundColor = UIColor().colorFromHexString("#005BAA")
            cell.titleGenre.textColor = UIColor().colorFromHexString("#FFFFFE")
        }
        else
        {
            cell.titleBackground.backgroundColor = UIColor().colorFromHexString("#ECECEC")
            cell.titleGenre.textColor = UIColor().colorFromHexString("#9B9B9B")
        }
        cell.configure(with: genre)
        return cell
    }
    
    func didSelect(collectionView: UICollectionView, indexPath: IndexPath) {
        let selectedGenre = entities.genres[indexPath.row]
        presenter?.didSelect(selectedGenre)
        selectedIndex = indexPath.row
        collectionView.reloadData()
    }
    
}
