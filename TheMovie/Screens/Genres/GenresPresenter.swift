//
//  GenresPresenter.swift
//  TheMovie
//
//  Created by Denny Purwana on 2/10/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import Foundation

typealias GenresPresenterDependencies = (
    interactor: GenresInteractor,
    router: GenresRouterOutput
)

final class GenresPresenter: Presenterable {

    internal var entities: GenreEntities!
    private weak var view: GenresViewInputs!
    let dependencies: GenresPresenterDependencies

    init(entities: GenreEntities,
         view: GenresViewInputs,
         dependencies: GenresPresenterDependencies)
    {
        self.view = view
        self.entities = entities
        self.dependencies = dependencies
    }

}

extension GenresPresenter: GenresViewOutputs {
    func viewDidLoad() {
        view.configure(entities: entities)
        entities.genreApiState.isFetching = true
        dependencies.interactor.loadGenres()
    }

}

extension GenresPresenter: GenresInteractorOutputs {
    
    func onSuccess(datas: Genres) {
        entities.genreApiState.isFetching = false
        entities.genreApiState.pageCount += 1
        entities.genres += datas.genres ?? []
        view.reloadCollectionView(collectionViewDataSource: GenresCollectionViewDataSource(entities: entities, presenter: self))
    }
    
    func onError(error: Error) {
        view.indicatorView(animate: false)
    }
    
}

extension GenresPresenter: GenresCollectionViewDataSourceOutputs {
   
    func didSelect(_ genre: Genre) {
        print("GENRE ITEM CLICKED : \(genre.name)")
    }
    
}
