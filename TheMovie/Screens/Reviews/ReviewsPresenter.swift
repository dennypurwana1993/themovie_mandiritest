//
//  ReviewsPresenter.swift
//  TheMovie
//
//  Created by Denny Purwana on 2/5/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import Foundation
import UIKit

class ReviewsPresenter: NSObject {
    
    private let service: ReviewsService
    weak private var view: ReviewsView?
    init(service: ReviewsService) {
        self.service = service
    }
    
    func attachView(view: ReviewsView) {
        self.view = view
    }
    
    func detachView() {
        view = nil
    }
    
    func loadAllReviews(movieId: Int) {
       self.view?.startLoading()
        self.service.loadAllReviews(movieID: movieId,
            successCallback: {(data) in
                self.view?.showAllReviews(list: data)
        },
            errorCallback: { (errorMessage) in
                self.view?.stopLoading()
        })
    }
    
       
}
