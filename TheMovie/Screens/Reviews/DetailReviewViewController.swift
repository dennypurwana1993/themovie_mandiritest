//
//  DetailReviewViewController.swift
//  TheMovie
//
//  Created by Denny Purwana on 2/5/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import UIKit

class DetailReviewViewController: UIViewController {

    @IBOutlet weak var iconBack: UIImageView!
    @IBOutlet weak var contentReviewLbl: UILabel!
    @IBOutlet weak var createdAtLbl: UILabel!
    @IBOutlet weak var movieTitleLbl: UILabel!
    var titleMovie: String = ""
    var username: String = ""
    var createdAt: String = ""
    var content: String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    func setupView (){
        movieTitleLbl.text = titleMovie
        createdAtLbl.text = "Written by \(username). on \(createdAt)"
        contentReviewLbl.text = content
        let gestureBack = UITapGestureRecognizer(target: self, action:  #selector(self.back))
        iconBack.isUserInteractionEnabled = true
        iconBack.addGestureRecognizer(gestureBack)
    }
    
    @objc func back(){
        //self.dismiss(animated: true, completion: nil)
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromLeft
        transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
        self.dismiss(animated: false, completion: nil)
    }

}


extension UIViewController {
    
    func showDetailReviewController(title: String, username: String, cratedAt: String, content: String) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Dashboard", bundle:nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "detailReview") as! DetailReviewViewController
        vc.hidesBottomBarWhenPushed = true
        vc.modalPresentationStyle = .fullScreen
        vc.titleMovie = title
        vc.username = username
        vc.createdAt = cratedAt
        vc.content = content
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromRight
        transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)

        present(vc, animated: false, completion: nil)
    }
    
}
