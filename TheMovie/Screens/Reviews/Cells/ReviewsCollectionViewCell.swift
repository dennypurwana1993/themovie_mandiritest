//
//  ReviewsCollectionViewCell.swift
//  TheMovie
//
//  Created by Denny Purwana on 2/5/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import UIKit

class ReviewsCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var backgroundReview: UIView!
    @IBOutlet weak var readAllLbl: UILabel!
    @IBOutlet weak var contentLbl: UILabel!
    @IBOutlet weak var createdAtLbl: UILabel!
    @IBOutlet weak var authorNameLbl: UILabel!
    @IBOutlet weak var authorAvatar: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    public func configure(with data: Review) {
       
       
        authorAvatar.kf.setImage(with: URL(string: data.author_details?.avatar_path ?? ""), placeholder: UIImage(named: "worker"))
        contentLbl.text = data.content
        authorNameLbl.text = data.author_details?.username
        createdAtLbl.text = data.created_at
        backgroundReview.layer.cornerRadius = 10
        backgroundReview.layer.masksToBounds = true
        
    }
    
    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        setNeedsLayout()
        layoutIfNeeded()
        let size = contentView.systemLayoutSizeFitting(layoutAttributes.size)
        var frame  = layoutAttributes.frame
        //        frame.size.height = ceil(size.height)
        //        frame.size.width = ceil(size.width)
        layoutAttributes.frame = frame
        return layoutAttributes
    }
    
}
