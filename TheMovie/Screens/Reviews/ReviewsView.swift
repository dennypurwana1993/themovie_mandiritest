//
//  ReviewsView.swift
//  TheMovie
//
//  Created by Denny Purwana on 2/5/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import Foundation
protocol ReviewsView : NSObjectProtocol {
  
    func showAllReviews(list: [Review]?)
    func startLoading()
    func stopLoading()
    func failed(code: String)
    
}
