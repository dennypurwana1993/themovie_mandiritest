//
//  ReviewsViewController.swift
//  TheMovie
//
//  Created by Denny Purwana on 2/5/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import UIKit

class ReviewsViewController: UIViewController , ReviewsView
,UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout{
    
    @IBOutlet weak var movieRelease: UILabel!
    @IBOutlet weak var movieName: UILabel!
    @IBOutlet weak var iconBack: UIImageView!
    @IBOutlet weak var reviewsCollectionView: UICollectionView!
    var presenter = ReviewsPresenter(service: ReviewsService())
    var genreArray : [String] = []
    var reviews: [Review] = []
    var movieId = 0
    var movieTitle = ""
    var movieReleaseVal = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
        setupCollectionView()
        presenter.loadAllReviews(movieId: movieId)
    }
    
    func showAllReviews(list: [Review]?) {
        if(list!.count > 0){
            self.reviews = list ?? []
            self.reviewsCollectionView.reloadData()
        }
    }
    
    func startLoading() {
        
    }
    
    func stopLoading() {
        
    }
    
    func failed(code: String) {
        
    }
    
    func setupCollectionView(){
        movieName.text = movieTitle
        movieRelease.text = movieReleaseVal
        registerNib(collectionView: reviewsCollectionView, nibName:"ReviewsCollectionViewCell", identifierName:"reviewsCell")
        let gestureBack = UITapGestureRecognizer(target: self, action:  #selector(self.back))
        iconBack.isUserInteractionEnabled = true
        iconBack.addGestureRecognizer(gestureBack)
    }
    
    @objc func back(){
        //self.dismiss(animated: true, completion: nil)
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromLeft
        transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
        self.dismiss(animated: false, completion: nil)
    }
    
    
    func registerNib(collectionView:UICollectionView, nibName:String, identifierName:String){
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib.init(nibName: nibName, bundle: nil), forCellWithReuseIdentifier: identifierName)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return reviews.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "reviewsCell", for: indexPath) as! ReviewsCollectionViewCell
        let data = reviews[indexPath.row]
        cell.configure(with: data)
        return cell
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let data = reviews[indexPath.row]
        showDetailReviewController(title: movieTitle, username: data.author_details?.username ?? "", cratedAt: data.created_at ?? "", content: data.content ?? "" )
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: (collectionView.bounds.width), height: 250 )
        
    }
    
    
}


extension UIViewController {
    
    func showAllReviewsController(movieId: Int, movieTitle: String, overview: String) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Dashboard", bundle:nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "reviews") as! ReviewsViewController
        vc.hidesBottomBarWhenPushed = true
        vc.modalPresentationStyle = .fullScreen
        vc.movieId = movieId
        vc.movieTitle = movieTitle
        vc.movieReleaseVal = overview
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromRight
        transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
        
        present(vc, animated: false, completion: nil)
    }
    
}
