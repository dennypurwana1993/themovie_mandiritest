//
//  DashboardPresenter.swift
//  TheMovie
//
//  Created by Denny Purwana on 2/5/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import Foundation
import UIKit

class DashboardPresenter: NSObject {
    
    private let service: DashboardService
    private let moviesService: MoviesService
    weak private var view: DashboardView?
    init(service: DashboardService, moviesService: MoviesService) {
        self.service = service
        self.moviesService = moviesService
    }
    
    func attachView(view: DashboardView) {
        self.view = view
    }
    
    func detachView() {
        view = nil
    }
    
    func loadGenres() {
       self.view?.startLoading()
        self.service.loadGenres(
            successCallback: {(data) in
                self.view?.stopLoading()
                self.view?.showGenres(list: data)
        },
            errorCallback: { (errorMessage) in
                self.view?.stopLoading()
        })
    }
    
    func loadPopularMovie() {
       self.view?.startLoading()
        self.moviesService.loadPopular(
            successCallback: {(data) in
                self.view?.showPopularMovie(list: data)
                self.loadGenres()
        },
            errorCallback: { (errorMessage) in
                self.view?.stopLoading()
        })
    }
    
    
    func loadDiscoverMovie(genre: Int, page: Int) {
       self.view?.startLoading()
        self.moviesService.loadDiscoverMovieByGenre(genre: genre, page: page,
            successCallback: {(data, totalPage) in
                self.view?.showDiscoverMovie(list: data)
        },
            errorCallback: { (errorMessage) in
                self.view?.stopLoading()
        })
    }
       
}
