//
//  DashboardView.swift
//  TheMovie
//
//  Created by Denny Purwana on 2/5/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import Foundation
protocol DashboardView : NSObjectProtocol {
  
    func showGenres(list: [Genre]?)
    func showPopularMovie(list: [Movie]?)
    func showDiscoverMovie(list: [Movie]?)
    func startLoading()
    func stopLoading()
    func failed(code: String)
    
}
