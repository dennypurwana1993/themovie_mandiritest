//
//  DashboardViewController.swift
//  TheMovie
//
//  Created by Denny Purwana on 2/5/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import UIKit

class DashboardViewController: UIViewController ,DashboardView
,UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout{
    
    @IBOutlet weak var btnAllMovie: UILabel!
    @IBOutlet weak var movieCollectionView: UICollectionView!
    @IBOutlet weak var popularMovieCollectionView: UICollectionView!
    @IBOutlet weak var genresCollectionView: UICollectionView!
    var presenter = DashboardPresenter(service: DashboardService(), moviesService: MoviesService())
    var genres : [Genre] = []
    var popularMovies: [Movie] = []
    var discoverMovies: [Movie] = []
    var selectedIndex = Int ()
    var genreId : Int = 28
    var genreName : String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
        setupCollectionView()
        presenter.loadPopularMovie()
        presenter.loadDiscoverMovie(genre: 28, page: 1)
    }
    
    func showPopularMovie(list: [Movie]?) {
        if(list!.count > 0){
            self.popularMovies = list ?? []
            self.popularMovieCollectionView.reloadData()
        }
    }
    
    func showDiscoverMovie(list: [Movie]?) {
        if(list!.count > 0){
            self.discoverMovies = list ?? []
            self.movieCollectionView.reloadData()
        }
    }
    
    func showGenres(list: [Genre]?) {
        if(list!.count > 0){
            self.genres = list ?? []
            self.genresCollectionView.reloadData()
        }
    }
    
    func startLoading() {
        
    }
    
    func stopLoading() {
        
    }
    
    func failed(code: String) {
        
    }
    
    func setupCollectionView(){
        
        registerNib(collectionView: popularMovieCollectionView, nibName:"PopularMovieCollectionViewCell", identifierName:"popularCell")
        registerNib(collectionView: movieCollectionView, nibName:"MovieCollectionViewCell", identifierName:"movieCell")
        registerNib(collectionView: genresCollectionView, nibName:"GenreCollectionViewCell", identifierName:"genreCell")
        let gestureToAllMovies = UITapGestureRecognizer(target: self, action:  #selector(self.openMovies))
        btnAllMovie.isUserInteractionEnabled = true
        btnAllMovie.addGestureRecognizer(gestureToAllMovies)
        
    }
    
    @objc func openMovies(){
//        showDiscoverMoviesByGenre(genre: genreId, name: genreName)
        print("SSSSS")
        GenresRouterInput().present(from: self)
    }
    
    
    func registerNib(collectionView:UICollectionView, nibName:String, identifierName:String){
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib.init(nibName: nibName, bundle: nil), forCellWithReuseIdentifier: identifierName)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var count : Int = 0
        if collectionView == genresCollectionView {
            count = genres.count
        } else if collectionView == popularMovieCollectionView {
            count = popularMovies.count
        } else if(collectionView == movieCollectionView) {
            count = discoverMovies.count
        }else {
            count = genres.count
        }
        
        return count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == popularMovieCollectionView {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "popularCell", for: indexPath) as! PopularMovieCollectionViewCell
            let movie = popularMovies[indexPath.row]
            cell.configure(with: movie)
            return cell
            
        } else if collectionView == movieCollectionView {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "movieCell", for: indexPath) as! MovieCollectionViewCell
            let movie = discoverMovies[indexPath.row]
            cell.configure(with: movie)
            return cell
            
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "genreCell", for: indexPath) as! GenreCollectionViewCell
            let genre = genres[indexPath.row]
            
            if selectedIndex == indexPath.row
            {
                cell.titleBackground.backgroundColor = UIColor().colorFromHexString("#005BAA")
                cell.titleGenre.textColor = UIColor().colorFromHexString("#FFFFFE")
            }
            else
            {
                cell.titleBackground.backgroundColor = UIColor().colorFromHexString("#ECECEC")
                cell.titleGenre.textColor = UIColor().colorFromHexString("#9B9B9B")
            }
            cell.configure(with: genre)
            
            return cell
            
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == genresCollectionView {
            selectedIndex = indexPath.row
            self.genresCollectionView.reloadData()
            let genre = genres[selectedIndex]
            genreId = genre.id ?? 0
            genreName = genre.name ?? ""
            presenter.loadDiscoverMovie(genre: genreId, page: 1)
            print(genre)
            
        } else if collectionView == popularMovieCollectionView {
            let movie = popularMovies[indexPath.item]
            print(movie.id)
            print(movie.name)
            showMovieDetailController(movieId: movie.id ?? 0)
        } else if collectionView == movieCollectionView {
            let movie = discoverMovies[indexPath.item]
            showMovieDetailController(movieId: movie.id ?? 0)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == popularMovieCollectionView {
            
            return CGSize(width: (collectionView.bounds.width / 2), height: collectionView.bounds.height )
            
        } else if collectionView == movieCollectionView {
            
            return CGSize(width: (collectionView.bounds.width), height: 250 )
            
        } else {
            let width = collectionView.bounds.width / 2
            return CGSize(width: width, height: 42 )
        }
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
}

extension DashboardViewController: Viewable {}
