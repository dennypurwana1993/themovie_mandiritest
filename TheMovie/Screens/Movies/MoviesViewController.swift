//
//  MoviesViewController.swift
//  TheMovie
//
//  Created by Denny Purwana on 2/5/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import UIKit
import UIScrollView_InfiniteScroll

class MoviesViewController: UIViewController ,MoviesView
,UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout{
    
    @IBOutlet weak var iconBack: UIImageView!
    @IBOutlet weak var genreNameLbl: UILabel!
    @IBOutlet weak var moviesCollectionView: UICollectionView!
    weak var refreshControl: UIRefreshControl!
    var presenter = MoviesPresenter(moviesService: MoviesService())
    var movies: [Movie] = []
    var tempMovies: [Movie] = []
    var totalPage = 0
    var page = 1
    var genreId = 0
    var genreName = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
        setupCollectionView()
        setupViews()
        refreshControl.beginRefreshing()
        presenter.loadDiscoverMovie(genre: genreId, page: page)
    }
    
    
    @objc func refresh(_ sender: UIRefreshControl) {
        print(page)
        presenter.loadDiscoverMovie(genre: genreId, page: page)
        
    }
    
    func setupViews() {
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        
        moviesCollectionView.addSubview(refreshControl)
        self.refreshControl = refreshControl
        
        moviesCollectionView.addInfiniteScroll { (collectionView) in
            self.presenter.loadDiscoverMovie(genre: 28, page: self.page + 1)
        }
        
        moviesCollectionView.setShouldShowInfiniteScrollHandler { (collectionView) -> Bool in
            return self.page < self.totalPage
        }
    }
    
    func showMovies(list: [Movie]?, totalPages: Int, page: Int) {
        
        if(list!.count > 0){
            
            if page == 1 {
                self.movies = list ?? []
                self.tempMovies = movies
            }
            else {
                self.movies = self.tempMovies
            }
            self.moviesCollectionView.reloadData()
            self.refreshControl.endRefreshing()
            self.moviesCollectionView.finishInfiniteScroll()
        }
    }
    
    
    func startLoading() {
        
    }
    
    func stopLoading() {
        
    }
    
    func failed(code: String) {
        
    }
    
    func setupCollectionView(){
        
        
        registerNib(collectionView: moviesCollectionView, nibName:"MovieCollectionViewCell", identifierName:"movieCell")
        genreNameLbl.text = genreName
        let gestureBack = UITapGestureRecognizer(target: self, action:  #selector(self.back))
        iconBack.isUserInteractionEnabled = true
        iconBack.addGestureRecognizer(gestureBack)
    }
    
    @objc func back(){
        //self.dismiss(animated: true, completion: nil)
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromLeft
        transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
        self.dismiss(animated: false, completion: nil)
    }
    
    
    func registerNib(collectionView:UICollectionView, nibName:String, identifierName:String){
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib.init(nibName: nibName, bundle: nil), forCellWithReuseIdentifier: identifierName)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "movieCell", for: indexPath) as! MovieCollectionViewCell
        let movie = movies[indexPath.row]
        cell.configure(with: movie)
        return cell
        
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let movie = movies[indexPath.item]
        showMovieDetailController(movieId: movie.id ?? 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: (collectionView.bounds.width), height: 250 )
        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
}

extension UIViewController {
    
    func showDiscoverMoviesByGenre(genre: Int, name: String) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Dashboard", bundle:nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "movies") as! MoviesViewController
        vc.hidesBottomBarWhenPushed = true
        vc.modalPresentationStyle = .fullScreen
        vc.genreId = genre
        vc.genreName = name
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromRight
        transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
        
        present(vc, animated: false, completion: nil)
    }
    
}
