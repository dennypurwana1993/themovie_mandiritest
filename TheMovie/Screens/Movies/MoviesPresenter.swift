//
//  MoviesPresenter.swift
//  TheMovie
//
//  Created by Denny Purwana on 2/5/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import Foundation
import UIKit

class MoviesPresenter: NSObject {
    
    private let moviesService: MoviesService
    weak private var view: MoviesView?
    init(moviesService: MoviesService) {
        self.moviesService = moviesService
    }
    
    func attachView(view: MoviesView) {
        self.view = view
    }
    
    func detachView() {
        view = nil
    }
    
    func loadDiscoverMovie(genre: Int, page: Int) {
       self.view?.startLoading()
        self.moviesService.loadDiscoverMovieByGenre(genre: genre, page: page,
            successCallback: {(data,totalPages) in
                self.view?.showMovies(list: data, totalPages: totalPages, page: page)
        },
            errorCallback: { (errorMessage) in
                self.view?.stopLoading()
        })
    }
    
       
}
