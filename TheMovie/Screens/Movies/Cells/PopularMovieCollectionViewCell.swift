//
//  PopularMovieCollectionViewCell.swift
//  TheMovie
//
//  Created by Denny Purwana on 2/5/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import UIKit
import Kingfisher
class PopularMovieCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var moviePoster: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    public func configure(with movie: Movie) {
        let imageUrl =  "\(Config.BaseUrlImages)\(movie.poster_path ?? "")"
        moviePoster.kf.setImage(with: URL(string: imageUrl), placeholder: UIImage(named: "clapperboard"))
        moviePoster.makeRoundCorners(byRadius: 10)
        
    }
    
    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        setNeedsLayout()
        layoutIfNeeded()
        let size = contentView.systemLayoutSizeFitting(layoutAttributes.size)
        var frame  = layoutAttributes.frame
//        frame.size.height = ceil(size.height)
//        frame.size.width = ceil(size.width)
        layoutAttributes.frame = frame
        return layoutAttributes
    }
    
}
