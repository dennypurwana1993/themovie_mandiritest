//
//  MoviesService.swift
//  TheMovie
//
//  Created by Denny Purwana on 2/5/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import Foundation
import RxSwift
import Kingfisher

class MoviesService: NSObject {
    
    let disposeBag = DisposeBag()
    
    func loadPopular(successCallback: ((_ result: [Movie]?) -> Void)?,
                     errorCallback: ((_ code: String) -> Void)?) {
        provider.rx.request(.popular)
            .subscribe { (event) in
                switch event {
                case .success(let result):
                    do {
                        if let JSONString = String(data: result.data, encoding: String.Encoding.utf8) {
                            //print(JSONString)
                        }
                        let response = try JSONDecoder().decode(Movies.self, from: result.data)
                        print(response)
                        successCallback?(response.results)
                    }
                    catch {
                        print(error.localizedDescription)
                    }
                case .error(let error):
                    print(error.localizedDescription)
                }
        }
        .disposed(by: disposeBag)
    }
    
    
    func loadDiscoverMovieByGenre(genre:Int, page: Int, successCallback: ((_ result: [Movie]?, _ totalPages: Int) -> Void)?,
                                  errorCallback: ((_ code: String) -> Void)?) {
        provider.rx.request(.discoverMovie(genre,page))
            .subscribe { (event) in
                switch event {
                case .success(let result):
                    do {
                        if let JSONString = String(data: result.data, encoding: String.Encoding.utf8) {
                            print(JSONString)
                        }
                        let response = try JSONDecoder().decode(Movies.self, from: result.data)
                        print(response)
                        successCallback?(response.results, response.total_pages ?? 0)
                    }
                    catch {
                        print(error.localizedDescription)
                    }
                case .error(let error):
                    print(error.localizedDescription)
                }
        }
        .disposed(by: disposeBag)
    }
    
}
