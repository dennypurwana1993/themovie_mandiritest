//
//  MoviesView.swift
//  TheMovie
//
//  Created by Denny Purwana on 2/5/21.
//  Copyright © 2021 Denny Purwana. All rights reserved.
//

import Foundation
protocol MoviesView : NSObjectProtocol {
  
    func showMovies(list: [Movie]?, totalPages: Int, page: Int)
    func startLoading()
    func stopLoading()
    func failed(code: String)
    
}
